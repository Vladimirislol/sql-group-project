package com.sql;

public class Customers {
    private int customerId;
    private String fName;
    private String lName;
    private String email;
    private CustomersAddresses address;

    public Customers(int customerId, String fName, String lName, String email, CustomersAddresses address) {
        this.customerId = customerId;
        this.fName = fName;
        this.lName = lName;
        this.email = email;
        this.address = new CustomersAddresses(address.getStreet(), address.getCity(), address.getCity(),
                address.getProvince());
    }

    public int getCustomerId() {
        return this.customerId;
    }

    public String getfName() {
        return this.fName;
    }

    public String getlName() {
        return this.lName;
    }

    public String getEmail() {
        return this.email;
    }

    @Override
    public String toString() {
        return "Customers: [customerId=" + customerId + ", fName=" + fName + ", lName=" + lName + ", email=" + email
                + "]";
    }

}
