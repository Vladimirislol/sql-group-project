package com.sql;

public class WarehousesAddresses {
    private String street;
    private String city;
    private String province;
    private String country;
    public static final String SQL_TYPE_NAME = "pj_warehouses_addresses";

    public WarehousesAddresses(String street, String city, String province, String country) {
        this.street = street;
        this.city = city;
        this.province = province;
        this.country = country;
    }

    public String getStreet() {
        return this.street;
    }

    public String getCity() {
        return this.city;
    }

    public String getProvince() {
        return this.province;
    }

    public String getCountry() {
        return this.country;
    }

}
