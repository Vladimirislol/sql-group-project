package com.sql;

public class Warehouses {
    private int wId;
    private String wName;
    public static final String SQL_TYPE_NAME = "pj_warehouses";

    public Warehouses(int wId, String wName) {
        this.wId = wId;
        this.wName = wName;
    }

    public int getWarehouseId() {
        return this.wId;
    }

    public String getWarehouseName() {
        return this.wName;
    }

    @Override
    public String toString() {
        return "Warehouses: [warehouse id=" + wId + ", warehouse name =" + wName + "]";
    }

}
