package com.sql;

import java.sql.*;
import java.util.Map;
import java.util.Scanner;

/**
 * Hello world!
 *
 */
public class App {

    private String username;
    private String password;
    private String connectionUrl;
    private final Scanner reader;

    public App() {
        reader = new Scanner(System.in);

        username = "";
        password = "";
        connectionUrl = "jdbc:oracle:thin:@198.168.52.211:1521/pdbora19c.dawsoncollege.qc.ca";
    }

    public static void main(String[] args) throws ClassNotFoundException{
        App app = new App();

        app.getUserLoginInfo();

        try (Connection connection = DriverManager.getConnection(app.connectionUrl, app.username, app.password)) {
            Map<String, Class<?>> map = connection.getTypeMap();
            // next line makes a connection with the WarehousesAddresses.java file and the SQL table pj_warehouses_addresses
            map.put(WarehousesAddresses.SQL_TYPE_NAME, Class.forName("com.sql.WarehousesAddresses"));
            map.put(Warehouses.SQL_TYPE_NAME, Class.forName("com.sql.Warehouses"));
            

        }  catch(SQLException e) {
            System.out.println("invalid login info");
            e.printStackTrace();
        }


        Scanner reader = new Scanner(System.in);
        System.out.println("Hello World!");

        System.out.println("Enter the Id: ");
        int customerId = reader.nextInt();

        reader.nextLine();

        System.out.println("Enter the fName: ");
        String fName = reader.nextLine();

        System.out.println("Enter the lName: ");
        String lName = reader.nextLine();

        System.out.println("Enter the email: ");
        String email = reader.nextLine();

        Customers c = new Customers(customerId, fName, lName, email, null);

        System.out.println(c.getEmail());
    }

    public void getUserLoginInfo() {
        System.out.print("\nUsername: ");
        username = reader.nextLine();
        System.out.println();

        System.out.print("Password: ");
        password = new String(System.console().readPassword());
        System.out.println("\n");
    }
}
