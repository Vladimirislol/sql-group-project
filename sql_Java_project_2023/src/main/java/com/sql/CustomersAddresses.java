package com.sql;

public class CustomersAddresses {
    private String street;
    private String city;
    private String province;
    private String country;

    public CustomersAddresses(String street, String city, String province, String country) {
        this.street = street;
        this.city = city;
        this.province = province;
        this.country = country;
    }

    public String getStreet() {
        return street;
    }

    public String getCity() {
        return city;
    }

    public String getProvince() {
        return province;
    }

    public String getCountry() {
        return country;
    }

}
