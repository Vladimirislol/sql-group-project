-- dropping tables
DROP TABLE pj_customers CASCADE CONSTRAINTS;
DROP TABLE pj_customers_addresses CASCADE CONSTRAINTS;
DROP TABLE pj_stores CASCADE CONSTRAINTS;
DROP TABLE pj_products CASCADE CONSTRAINTS;
DROP TABLE pj_orders CASCADE CONSTRAINTS;
DROP TABLE pj_reviews CASCADE CONSTRAINTS;
DROP TABLE pj_warehouses CASCADE CONSTRAINTS;
DROP TABLE pj_warehouses_addresses CASCADE CONSTRAINTS;
DROP TABLE pj_product_warehouse CASCADE CONSTRAINTS;

-- creating tables for customers name 
CREATE TABLE pj_customers (
	customer_id NUMBER PRIMARY KEY,
	fname VARCHAR2(100) NOT NULL,
	lname VARCHAR2(100) NOT NULL,
	email VARCHAR2(100) NOT NULL
);

-- creating tables for customers address
CREATE TABLE pj_customers_addresses (
	customer_id NUMBER REFERENCES pj_customers(customer_id),
    	street VARCHAR2(100),
	city VARCHAR2(100),
	province VARCHAR2(100),
	country VARCHAR2(100)
);
  
-- a table for the stores names and ID
CREATE TABLE pj_stores (
	store_id NUMBER PRIMARY KEY,
	store_name_ordered VARCHAR2(100) NOT NULL
);

-- a table for products name, category, and ID
CREATE TABLE pj_products (
	product_id NUMBER PRIMARY KEY,
	product_name VARCHAR2(100) NOT NULL,
	product_category VARCHAR2(100) NOT NUll
);
 
-- a table for orders that were placed by customers
CREATE TABLE pj_orders (
	order_id NUMBER PRIMARY KEY,
	customer_id NUMBER REFERENCES pj_customers(customer_id),
	store_id NUMBER REFERENCES pj_stores(store_id),
	product_id NUMBER REFERENCES pj_products(product_id),
	numberOfItems NUMBER NOT NULL,
	order_date DATE,
	price NUMBER NOT NULL
);

-- a table for the reviews that were given to a specif product by customers 
Create TABLE pj_reviews (
	review_id NUMBER PRIMARY KEY,
	customer_id NUMBER REFERENCES pj_customers(customer_id),
	product_id NUMBER REFERENCES pj_products(product_id),
	review_flag CHAR(1),
	review_description VARCHAR2(100),
	rating_number NUMBER
);

-- a table for warehouse name and ID
CREATE TABLE pj_warehouses (
	warehouse_id NUMBER PRIMARY KEY,
	name VARCHAR2(100) NOT NULL
);

-- a table for warehouse address
CREATE TABLE pj_warehouses_addresses (
	street VARCHAR2(100) NOT NULL,
	city VARCHAR2(100) NOT NULL,
	province VARCHAR2(100) NOT NULL,
	country VARCHAR2(100) NOT NULL,
	warehouse_id NUMBER REFERENCES pj_warehouses(warehouse_id)
);

-- a table for warehouse connecting to products and the quantity of product
CREATE TABLE pj_product_warehouse (
	warehouse_id NUMBER REFERENCES pj_warehouses(warehouse_id),
	product_id NUMBER REFERENCES pj_products(product_id),
	quantity NUMBER NOT NULL
);