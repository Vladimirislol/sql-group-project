-- inserting into customers
INSERT INTO pj_customers(customer_id, fname, lname, email) VALUES
    (1, 'mahsa', 'sadeghi', 'msadeghi@dawsoncollege.qc.ca');
    INSERT INTO pj_customers(customer_id, fname, lname, email) VALUES
    (2, 'alex', 'brown', 'alex@gmail.com');
    INSERT INTO pj_customers(customer_id, fname, lname, email) VALUES
    (3, 'martin', 'alexandre', 'marting@yahoo.com');
    INSERT INTO pj_customers(customer_id, fname, lname, email) VALUES
    (4, 'daneil', 'hanne', 'daneil@yahoo.com');
    INSERT INTO pj_customers(customer_id, fname, lname, email) VALUES
    (5, 'alex', 'brown', 'alex@gmail.com');
    INSERT INTO pj_customers(customer_id, fname, lname, email) VALUES
    (6, 'John', 'boura', 'boura@gmail.com');
    INSERT INTO pj_customers(customer_id, fname, lname, email) VALUES
    (7, 'Ari', 'brown', 'b.a@gmail.com');
    INSERT INTO pj_customers(customer_id, fname, lname, email) VALUES
    (8, 'Amanda', 'Harry', 'am.harry@yahoo.com');
    INSERT INTO pj_customers(customer_id, fname, lname, email) VALUES
    (9, 'Jack', 'Jonhson', 'johnson.a@gmail.com');
    INSERT INTO pj_customers(customer_id, fname, lname, email) VALUES
    (10, 'mahsa', 'sadeghi', 'ms@@gmail.com');
    INSERT INTO pj_customers(customer_id, fname, lname, email) VALUES
    (11, 'John', 'belle', 'abcd@yahoo.com');
    INSERT INTO pj_customers(customer_id, fname, lname, email) VALUES
    (12, 'martin', 'Li', 'm.li@gmail.com');
    INSERT INTO pj_customers(customer_id, fname, lname, email) VALUES
    (13, 'olivia', 'smith','smith@gmail.com');
    INSERT INTO pj_customers(customer_id, fname, lname, email) VALUES
    (14, 'Noah', 'Garcia', 'g.noah@yahoo.com');

-- inserting into customers addresses
INSERT INTO pj_customers_addresses(customer_id, street, city, province, country) VALUES
	(1, 'dawson college', 'montreal', 'Quebec', 'Canada');
    INSERT INTO pj_customers_addresses(customer_id, street, city, province, country) VALUES
    (2, '090 boul saint laurent', 'montreal', 'Quebec', 'Canada');
    INSERT INTO pj_customers_addresses(customer_id, city, province, country) VALUES
    (3, 'brossard', 'Quebec', 'Canada');
    INSERT INTO pj_customers_addresses(customer_id, street, city, country) VALUES
    (4, '100 atwater street', 'toronto', 'Canada');
    INSERT INTO pj_customers_addresses(customer_id, street, city, province, country) VALUES
    (5,'boul saint laurent', 'montreal', 'Quebec', 'Canada');
	INSERT INTO pj_customers_addresses(customer_id, street, city, country) VALUES
    (6, '100 Young street', 'toronto', 'Canada');
	INSERT INTO pj_customers_addresses(customer_id) VALUES
    (7);
	INSERT INTO pj_customers_addresses(customer_id, street, city, province, country) VALUES
    (8, '100 boul saint laurent', 'montreal', 'Quebec', 'Canada');
	INSERT INTO pj_customers_addresses(customer_id, city, province, country) VALUES
    (9, 'Calgary', 'Alberta', 'Canada');
	INSERT INTO pj_customers_addresses(customer_id, street, city, country) VALUES
    (10, '104 gill street', 'toronto', 'Canada');
	INSERT INTO pj_customers_addresses(customer_id, street, city, country) VALUES
    (11, '105 Young street', 'toronto', 'Canada');
	INSERT INTO pj_customers_addresses(customer_id, street, city, province, country) VALUES
    (12, '87 boul saint laurent', 'montreal', 'Quebec','Canada');
	INSERT INTO pj_customers_addresses(customer_id, street, city, province, country) VALUES
    (13, '76 boul decalthon', 'Laval','Quebec','Canada');
	INSERT INTO pj_customers_addresses(customer_id, street, city, province, country) VALUES
    (14, '22222 happy street', 'Laval','Quebec','Canada');

-- inserting into products
INSERT INTO pj_products(product_id, product_name, product_category) VALUES(1, 'laptop asus 104s', 'electronics');
INSERT INTO pj_products(product_id, product_name, product_category) VALUES(2, 'apple', 'grocery');
INSERT INTO pj_products(product_id, product_name, product_category) VALUES(3, 'sims cd', 'video games');
INSERT INTO pj_products(product_id, product_name, product_category) VALUES(4, 'orange', 'grocery');
INSERT INTO pj_products(product_id, product_name, product_category) VALUES(5, 'barbie movie', 'dvd');
INSERT INTO pj_products(product_id, product_name, product_category) VALUES(6, 'loreal normal hair', 'health');
INSERT INTO pj_products(product_id, product_name, product_category) VALUES(7, 'bmw ix lego', 'toys');
INSERT INTO pj_products(product_id, product_name, product_category) VALUES(8, 'bmw i6', 'cars');
INSERT INTO pj_products(product_id, product_name, product_category) VALUES(9, 'truck 500c', 'vehicle');
INSERT INTO pj_products(product_id, product_name, product_category) VALUES(10, 'paper towel', 'beauty');
INSERT INTO pj_products(product_id, product_name, product_category) VALUES(11, 'plum', 'grocery');
INSERT INTO pj_products(product_id, product_name, product_category) VALUES(12, 'lamborghini lego', 'toys');
INSERT INTO pj_products(product_id, product_name, product_category) VALUES(13, 'chicken', 'grocery');
INSERT INTO pj_products(product_id, product_name, product_category) VALUES(14, 'pasta', 'grocery');
INSERT INTO pj_products(product_id, product_name, product_category) VALUES(15, 'ps5', 'electronics');
INSERT INTO pj_products(product_id, product_name, product_category) VALUES(16, 'tomato', 'grocery');
INSERT INTO pj_products(product_id, product_name, product_category) VALUES(17, 'Train X745', 'toys');

-- inserting into stores
INSERT INTO pj_stores(store_id, store_name_ordered) VALUES(1, 'marche adonis');
INSERT INTO pj_stores(store_id, store_name_ordered) VALUES(2, 'marche atwater');
INSERT INTO pj_stores(store_id, store_name_ordered) VALUES(3, 'dawson store');
INSERT INTO pj_stores(store_id, store_name_ordered) VALUES(4, 'store magic');
INSERT INTO pj_stores(store_id, store_name_ordered) VALUES(5, 'movie store');
INSERT INTO pj_stores(store_id, store_name_ordered) VALUES(6, 'super rue champlain');
INSERT INTO pj_stores(store_id, store_name_ordered) VALUES(7, 'toy r us');
INSERT INTO pj_stores(store_id, store_name_ordered) VALUES(8, 'dealer one');
INSERT INTO pj_stores(store_id, store_name_ordered) VALUES(9, 'dealer montreal');
INSERT INTO pj_stores(store_id, store_name_ordered) VALUES(10, 'movie start');
INSERT INTO pj_stores(store_id, store_name_ordered) VALUES(11, 'star store');

-- inserting into orders
INSERT INTO pj_orders(order_id, customer_id, store_id, product_id, numberOfItems, order_date, price) VALUES(1, 1, 1, 1, 1, to_date('2023-04-21', 'yyyy-mm-dd'), 970);
INSERT INTO pj_orders(order_id, customer_id, store_id, product_id, numberOfItems, order_date, price) VALUES(2, 2, 2, 2, 2, to_date('2023-10-23', 'yyyy-mm-dd'), 10);
INSERT INTO pj_orders(order_id, customer_id, store_id, product_id, numberOfItems, order_date, price) VALUES(3, 3, 3, 3, 3, to_date('2023-10-01', 'yyyy-mm-dd'), 50);
INSERT INTO pj_orders(order_id, customer_id, store_id, product_id, numberOfItems, order_date, price) VALUES(4, 4, 4, 4, 1, to_date('2023-10-23', 'yyyy-mm-dd'), 2);
INSERT INTO pj_orders(order_id, customer_id, store_id, product_id, numberOfItems, order_date, price) VALUES(5, 2, 5, 5, 1, to_date('2023-10-23', 'yyyy-mm-dd'), 30);
INSERT INTO pj_orders(order_id, customer_id, store_id, product_id, numberOfItems, order_date, price) VALUES(6, 3, 6, 6, 1, to_date('2023-10-10', 'yyyy-mm-dd'), 10);
INSERT INTO pj_orders(order_id, customer_id, store_id, product_id, numberOfItems, order_date, price) VALUES(7, 1, 7, 7, 1, to_date('2023-10-11', 'yyyy-mm-dd'), 40);
INSERT INTO pj_orders(order_id, customer_id, store_id, product_id, numberOfItems, order_date, price) VALUES(8, 6, 8, 8, 1, to_date('2023-10-10', 'yyyy-mm-dd'), 50000);
INSERT INTO pj_orders(order_id, customer_id, store_id, product_id, numberOfItems, order_date, price) VALUES(9, 9, 2, 11, 6, to_date('2020-05-06', 'yyyy-mm-dd'), 10);
INSERT INTO pj_orders(order_id, customer_id, store_id, product_id, numberOfItems, order_date, price) VALUES(10, 3, 6, 6, 3, to_date('2019-09-12', 'yyyy-mm-dd'), 30);
INSERT INTO pj_orders(order_id, customer_id, store_id, product_id, numberOfItems, order_date, price) VALUES(11, 1, 7, 12, 1, to_date('2010-10-11', 'yyyy-mm-dd'), 40);
INSERT INTO pj_orders(order_id, customer_id, store_id, product_id, numberOfItems, order_date, price) VALUES(12, 1, 2, 11, 7, to_date('2022-05-06', 'yyyy-mm-dd'), 10);
INSERT INTO pj_orders(order_id, customer_id, store_id, product_id, numberOfItems, order_date, price) VALUES(13, 1, 7, 12, 2, to_date('2023-10-07', 'yyyy-mm-dd'), 80);
INSERT INTO pj_orders(order_id, customer_id, store_id, product_id, numberOfItems, order_date, price) VALUES(14, 11, 8, 8, 1, to_date('2023-08-10', 'yyyy-mm-dd'), 50000);
INSERT INTO pj_orders(order_id, customer_id, store_id, product_id, numberOfItems, order_date, price) VALUES(15, 2, 5, 3, 1, to_date('2023-10-23', 'yyyy-mm-dd'), 16);
INSERT INTO pj_orders(order_id, customer_id, store_id, product_id, numberOfItems, order_date, price) VALUES(16, 2, 7, 5, 1, to_date('2023-10-02', 'yyyy-mm-dd'), 45);
INSERT INTO pj_orders(order_id, customer_id, store_id, product_id, numberOfItems, order_date, price) VALUES(17, 12, 1, 13, 1, to_date('2019-04-03', 'yyyy-mm-dd'), 9.5);
INSERT INTO pj_orders(order_id, customer_id, store_id, product_id, numberOfItems, order_date, price) VALUES(18, 13, 2, 14, 3, to_date('2021-12-29', 'yyyy-mm-dd'), 13.5);
INSERT INTO pj_orders(order_id, customer_id, store_id, product_id, numberOfItems, order_date, price) VALUES(19, 14, 11, 15, 1, to_date('2020-01-20', 'yyyy-mm-dd'), 200);
INSERT INTO pj_orders(order_id, customer_id, store_id, product_id, numberOfItems, order_date, price) VALUES(20, 1, 7, 7, 1, to_date('2022-10-11', 'yyyy-mm-dd'), 38);
INSERT INTO pj_orders(order_id, customer_id, store_id, product_id, numberOfItems, order_date, price) VALUES(21, 13, 4, 14, 3, to_date('2021-12-29', 'yyyy-mm-dd'), 15);

-- insertion for pj_warehouses
INSERT INTO pj_warehouses (warehouse_id, name) VALUES (1, 'Warehouse A');
INSERT INTO pj_warehouses (warehouse_id, name) VALUES (2, 'Warehouse B');
INSERT INTO pj_warehouses (warehouse_id, name) VALUES (3, 'Warehouse C');
INSERT INTO pj_warehouses (warehouse_id, name) VALUES (4, 'Warehouse D');
INSERT INTO pj_warehouses (warehouse_id, name) VALUES (5, 'Warehouse E');
INSERT INTO pj_warehouses (warehouse_id, name) VALUES (6, 'Warehouse F');

-- insertions for pj_warehouses_addresses
INSERT INTO pj_warehouses_addresses (street, city, province, country, warehouse_id) VALUES ('100 rue William', 'saint Laurent', 'Quebec', 'Canada', 1);
INSERT INTO pj_warehouses_addresses (street, city, province, country, warehouse_id) VALUES ('304 rue Francois-Perrault', 'Villera Saint-Michel', 'Quebec', 'Canada', 2);
INSERT INTO pj_warehouses_addresses (street, city, province, country, warehouse_id) VALUES ('86700 Weston rd', 'unknown', 'Toronto', 'Canada', 3);
INSERT INTO pj_warehouses_addresses (street, city, province, country, warehouse_id) VALUES ('170 Sideroad', 'Quebec City', 'Quebec', 'Canada', 4);
INSERT INTO pj_warehouses_addresses (street, city, province, country, warehouse_id) VALUES ('1231 Trudea road', 'unknown', 'Ottawa', 'Canada', 5);
INSERT INTO pj_warehouses_addresses (street, city, province, country, warehouse_id) VALUES ('16 Whitlock Rd', 'unknown', 'Alberta', 'Canada', 6);

-- insertion for pj_product_warehouse
INSERT INTO pj_product_warehouse (WAREHOUSE_ID, product_id, quantity) values (1, 1, 1000);
INSERT INTO pj_product_warehouse (WAREHOUSE_ID, product_id, quantity) values (2, 2, 24980);
INSERT INTO pj_product_warehouse (WAREHOUSE_ID, product_id, quantity) values (3, 3, 103);
INSERT INTO pj_product_warehouse (WAREHOUSE_ID, product_id, quantity) values (4, 4, 35405);
INSERT INTO pj_product_warehouse (WAREHOUSE_ID, product_id, quantity) values (5, 5, 40);
INSERT INTO pj_product_warehouse (WAREHOUSE_ID, product_id, quantity) values (6, 6, 450);
INSERT INTO pj_product_warehouse (WAREHOUSE_ID, product_id, quantity) values (1, 7, 10);
INSERT INTO pj_product_warehouse (WAREHOUSE_ID, product_id, quantity) values (1, 8, 6);
INSERT INTO pj_product_warehouse (WAREHOUSE_ID, product_id, quantity) values (5, 9, 1000);
INSERT INTO pj_product_warehouse (WAREHOUSE_ID, product_id, quantity) values (6, 10, 3532);
INSERT INTO pj_product_warehouse (WAREHOUSE_ID, product_id, quantity) values (3, 11, 43242);
INSERT INTO pj_product_warehouse (WAREHOUSE_ID, product_id, quantity) values (2, 10, 39484);
INSERT INTO pj_product_warehouse (WAREHOUSE_ID, product_id, quantity) values (4, 11, 6579);
INSERT INTO pj_product_warehouse (WAREHOUSE_ID, product_id, quantity) values (5, 12, 98765);
INSERT INTO pj_product_warehouse (WAREHOUSE_ID, product_id, quantity) values (6, 13, 43523);
INSERT INTO pj_product_warehouse (WAREHOUSE_ID, product_id, quantity) values (1, 14, 2132);
INSERT INTO pj_product_warehouse (WAREHOUSE_ID, product_id, quantity) values (4, 15, 123);
INSERT INTO pj_product_warehouse (WAREHOUSE_ID, product_id, quantity) values (1, 16, 352222);
INSERT INTO pj_product_warehouse (WAREHOUSE_ID, product_id, quantity) values (5, 17, 4543);

-- insertions for pj_reviews
INSERT INTO pj_reviews (review_id, customer_id, product_id, review_flag, review_description, rating_number) VALUES (1, 1, 1, 0, 'it was affordable.', 4);
INSERT INTO pj_reviews (review_id, customer_id, product_id, review_flag, review_description, rating_number) VALUES (2, 2, 2, 0, 'qualiy was not good', 3);
INSERT INTO pj_reviews (review_id, customer_id, product_id, review_flag, rating_number) VALUES (3, 3, 3, 1, 2);
INSERT INTO pj_reviews (review_id, customer_id, product_id, review_flag, review_description, rating_number) VALUES (4, 4, 4, 0, 'highly recommend', 5);
INSERT INTO pj_reviews (review_id, customer_id, product_id, review_flag, rating_number) VALUES (5, 5, 5, 0, 1);
INSERT INTO pj_reviews (review_id, customer_id, product_id, review_flag, review_description, rating_number) VALUES (6, 3, 6, 0, 'did not worth the price', 1);
INSERT INTO pj_reviews (review_id, customer_id, product_id, review_flag, review_description, rating_number) VALUES (7, 1, 7, 0, 'missing some parts', 1);
INSERT INTO pj_reviews (review_id, customer_id, product_id, review_flag, review_description, rating_number) VALUES (8, 6, 8, 1, 'trash', 5);
INSERT INTO pj_reviews (review_id, customer_id, product_id, rating_number) VALUES (9, 7, 9, 2);
INSERT INTO pj_reviews (review_id, customer_id, product_id, rating_number) VALUES (10, 8, 10, 5);
INSERT INTO pj_reviews (review_id, customer_id, product_id, rating_number) VALUES (11, 9, 11, 4);
INSERT INTO pj_reviews (review_id, customer_id, product_id, rating_number) VALUES (12, 3, 6, 3);
INSERT INTO pj_reviews (review_id, customer_id, product_id, review_flag, review_description, rating_number) VALUES (13, 1, 12, 0, 'missing some parts', 1);
INSERT INTO pj_reviews (review_id, customer_id, product_id, rating_number) VALUES (14, 1, 11, 4);
INSERT INTO pj_reviews (review_id, customer_id, product_id, review_flag, review_description, rating_number) VALUES (15, 10, 12, 0, 'great product', 1);
INSERT INTO pj_reviews (review_id, customer_id, product_id, review_flag, review_description, rating_number) VALUES (16, 11, 8, 1, 'bad quality', 5);
INSERT INTO pj_reviews (review_id, customer_id, product_id, review_flag, rating_number) VALUES (17, 5, 3, 0, 1);
INSERT INTO pj_reviews (review_id, customer_id, product_id, review_flag, rating_number) VALUES (18, 5, 5, 0, 4);
INSERT INTO pj_reviews (review_id, customer_id, product_id, rating_number) VALUES (19, 12, 13, 4);
INSERT INTO pj_reviews (review_id, customer_id, product_id, rating_number) VALUES (20, 13, 14, 5);
INSERT INTO pj_reviews (review_id, customer_id, product_id, review_flag, review_description, rating_number) VALUES (21, 1, 7, 2, 'worse car i have ever droven!', 1);
INSERT INTO pj_reviews (review_id, customer_id, product_id, rating_number) VALUES (22, 13, 14, 4);
INSERT INTO pj_reviews (review_id, customer_id, product_id) VALUES (23, 14, 15);


